<nav class="navbar navbar-expand-md navbar-dark" style="background: #EA2D2D; border-radius: 10px">

  <div class="container">

    <a href="#" class="navbar-brand">
      <img src="/img/logo.png" alt="" style="width: 50px; margin-right: 4px">
      <strong>
        SMK Mutiara Kota Bandung
      </strong>
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse flex justify-content-end navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2">Home</a></li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2">About</a></li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item"><a href="#" class="nav-link text-white mr-2">Contact</a></li>
      </ul>
      <ul class="navbar-nav" style="margin-right: 23px">
        <li class="nav-item">
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link text-white mr-2">Logout</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
        </li>
      </ul>
    </div>

  </div>

</nav>